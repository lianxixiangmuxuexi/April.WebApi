# April.WebApi

#### 介绍
这套主要是一个基于net core的简易版后台工程，集成了log4，Swagger，SqlSugar，缓存机制，后续会不断更新完善，但是可能是对于功能的尝试，所以如果以这套代码作为工程基础的朋友，提前做好**本地备份**

#### 软件架构
常规的三层架构，不过是简化版的


#### 博客地址

csdn：[net core Webapi基础工程搭建 总目录](https://blog.csdn.net/weixin_44518486/article/details/96482846)

博客园：[net core Webapi基础工程搭建 总目录](https://www.cnblogs.com/AprilBlank/p/11282181.html)

#### 求赞

如果这套基础工程对你有帮助，就给点个赞吧~

#### 其他说明

前端html页面，可以单独部署web（例如iis），单独建立站点，然后在appsettings配置白名单即可。

#### 更新日志


- 2019.10.29 发布的教程同步更新，多平台的部署问题可参考[多平台项目发布与部署](https://www.cnblogs.com/AprilBlank/p/11757027.html)
- 2019.10.09 从2.2升级到3.0，打算创建个新分支，为了方便查看不同点